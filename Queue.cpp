#include <stdio.h>
const int MAX_LENGTH = 100;
class Queue
{
	int value[MAX_LENGTH];
	int first;
	int last; //номер за последним
public:
	Queue();
	~Queue();
	/*Queue(const Queue &other);*/
	void pushTail(int v);
	int pop();
	int isEmpty();
	void show();
	bool remove(int number);
};

Queue::Queue() {
	first = 0;
	last = 0;
}

Queue::~Queue() {

}

/*Queue::Queue(const Queue &other) {

}*/

void Queue::pushTail(int v) {
	if (last + 1 >= MAX_LENGTH) {
		printf("Queue if full\n");
		return;
	}
	value[last] = v;
	last++;
}

int Queue::pop() {
	if (!this->isEmpty()) {
		int r = value[first];
		first++;
		return r;
	} else {
		printf("Queue is empty\n");
	}
}

int Queue::isEmpty() {
	return (first >= last);
}

void Queue::show() {
	printf("Head\n");
	for (int i = first; i < last; ++i)
	{
		printf("%i\n", this->value[i]);
	}
	printf("Tail\n\n");
}

bool Queue::remove(int number) {
	if (first + number > last) {
	/*	printf("Invalid number\n");*/
		return 0;
	}

	for (int i = first + number; i < last; ++i)
	{
		this->value[i] = this->value[i+1];
	}
	last--;
}