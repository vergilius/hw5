#include "Queue.cpp"
#include <iostream>

void remove_odd(Queue *q) {
	for (int i = 1; q->remove(i); i++) {}
}

int main(int argc, char const *argv[])
{
	Queue *q = new Queue();
	for (int i = 0; i < 10; i++) {
		q->pushTail(i);
	}
	q->show();
	remove_odd(q);
	q->show();

	delete q;
	return 0;
}